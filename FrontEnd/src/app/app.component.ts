import { AppService } from './services/app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private service: AppService) { }

  title = 'Crypto Currency Dashboard';
  public data;
  public OverviewData;

  ngOnInit(): void {
    this.getOverview();
    setInterval(() => {
      this.getOverview();
    }, 5000);

    // Not in use
    // this.service.send();
    // console.log('Started');
    // this.service.receiveData().subscribe(
    //   (data: any) => {
    //     this.data = data;
    //     console.log(data);
    //   }
    // );
  }

  public getOverview(): any {
    this.service.getOverview().subscribe(
      (res: any) => {
        this.OverviewData = res;
      }
    );
  }
}
