import { HistoryComponent } from './components/history/history.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './components/overview/overviewcomponent';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full',
  },
  { path: 'overview', component: OverviewComponent },
  { path: 'history/:currency', component: HistoryComponent },
  {
    path: '**',
    redirectTo: 'overview'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes)
  ],

})
export class AppRoutingModule { }
