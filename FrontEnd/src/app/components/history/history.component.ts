import { TimeFrames, HistoryPayload } from './../../models/history.model';
import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})

export class HistoryComponent implements OnInit {
  constructor(
    private activeRoute: ActivatedRoute,
    private service: AppService
  ) { }

  ControlsForm: FormGroup;

  timeframe = 'histoday';
  currency = 'USD';
  limit = '20';
  crypto;

  timeFrames: TimeFrames[] = [
    { type: 'Daily', value: 'histoday' },
    { type: 'Hourly', value: 'histohour' },
    { type: 'Minute', value: 'histominute' },
  ];
  currencies = ['USD', 'GBP', 'EUR', 'JPY', 'ZAR'];
  limits = ['10', '20', '30', '40', '50', '100'];

  lineChartData: ChartDataSets[] = [
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
    { data: [], label: '' },
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'none',
      borderWidth: 1,
      backgroundColor: 'rgba(51, 153, 255, 0.28)',
    },
    {
      borderColor: 'none',
      borderWidth: 1,
      backgroundColor: 'rgba(0, 204, 0, 0.28)',
    },
    {
      borderColor: 'none',
      borderWidth: 1,
      backgroundColor: 'rgba(255, 102, 0, 0.28)',
    },
    {
      borderColor: 'none',
      borderWidth: 1,
      backgroundColor: 'rgba(255, 0, 0, 0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  ngOnInit(): void {
    this.ControlsForm = new FormGroup({
      currency: new FormControl(this.currency),
      limit: new FormControl(this.limit),
      timeframe: new FormControl(this.timeframe),
    });
    this.activeRoute.params.subscribe(params =>
      this.crypto = params.currency
    );

    this.update();
  }

  update(): void {
    this.lineChartData[0].data = [];
    this.lineChartData[1].data = [];
    this.lineChartData[2].data = [];
    this.lineChartData[3].data = [];
    this.lineChartLabels = [];

    const data: HistoryPayload = {
      period: this.timeframe === undefined ? 'histoday' : this.timeframe,
      currency: this.currency === undefined ? 'USD' : this.currency,
      crypto: this.crypto,
      limit: this.limit === undefined ? '20' : this.limit
    };

    this.service.getHistory(data).subscribe(
      (res: any) => {
        for (const i of res.Data.Data) {
          this.lineChartLabels.push(new Date(i.time * 1000).toUTCString());

          this.lineChartData[0].data.push(i.high);
          this.lineChartData[0].label = 'High';

          this.lineChartData[1].data.push(i.low);
          this.lineChartData[1].label = 'Low';

          this.lineChartData[2].data.push(i.open);
          this.lineChartData[2].label = 'Open';

          this.lineChartData[3].data.push(i.close);
          this.lineChartData[3].label = 'Close';
        }
      }
    );
  }
}
