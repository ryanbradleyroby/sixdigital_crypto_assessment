import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { Overview } from '../../models/overview.model';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})

export class OverviewComponent implements OnInit {
  public SearchString;
  public DATA: Overview[] = [];
  public interval;

  constructor(
    private route: Router,
    private service: AppService
  ) { }

  displayedColumns: string[] = ['name', 'dollar', 'pound', 'euro', 'yen', 'rand', 'action'];
  dataSource = new MatTableDataSource(this.DATA);
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  applyFilter(filterValue: string): any {
    if (filterValue.length > 0) {
      clearInterval(this.interval);
    } else {
      this.interval = setInterval(() => {
        this.DATA = [];
        this.getOverview();
      }, 10000);
    }
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  public viewHistory(symbol): void {
    this.route.navigate(['/history/' + symbol]);
  }

  public getOverview(): any {
    this.service.getOverview().subscribe(
      (res: any) => {
        this.formatData(res);
      }
    );
  }

  private formatData(data): void {
    this.DATA.push({
      name: 'Bitcoin (BTC)',
      dollar: data.BTC.USD,
      pound: data.BTC.GBP,
      euro: data.BTC.EUR,
      yen: data.BTC.JPY,
      rand: data.BTC.ZAR,
      symbol: 'BTC'
    });

    this.DATA.push({
      name: 'Etherium (ETH)',
      dollar: data.ETH.USD,
      pound: data.ETH.GBP,
      euro: data.ETH.EUR,
      yen: data.ETH.JPY,
      rand: data.ETH.ZAR,
      symbol: 'ETH'
    });

    this.DATA.push({
      name: 'XRP (XRP)',
      dollar: data.XRP.USD,
      pound: data.XRP.GBP,
      euro: data.XRP.EUR,
      yen: data.XRP.JPY,
      rand: data.XRP.ZAR,
      symbol: 'XRP'
    });

    this.DATA.push({
      name: 'Litecoin (LTC)',
      dollar: data.LTC.USD,
      pound: data.LTC.GBP,
      euro: data.LTC.EUR,
      yen: data.LTC.JPY,
      rand: data.LTC.ZAR,
      symbol: 'LTC'
    });

    this.DATA.push({
      name: 'Bitcoin Cash (BCH)',
      dollar: data.BCH.USD,
      pound: data.BCH.GBP,
      euro: data.BCH.EUR,
      yen: data.BCH.JPY,
      rand: data.BCH.ZAR,
      symbol: 'BCH'
    });

    this.DATA.push({
      name: 'Etherium Classic (ETC)',
      dollar: data.ETC.USD,
      pound: data.ETC.GBP,
      euro: data.ETC.EUR,
      yen: data.ETC.JPY,
      rand: data.ETC.ZAR,
      symbol: 'ETC'
    });

    this.dataSource = new MatTableDataSource(this.DATA);
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.getOverview();
    this.interval = setInterval(() => {
      this.DATA = [];
      this.getOverview();
    }, 10000);
  }
}
