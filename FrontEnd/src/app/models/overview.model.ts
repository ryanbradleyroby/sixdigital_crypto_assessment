export interface Overview {
  name: string;
  dollar: number;
  pound: number;
  euro: number;
  yen: number;
  rand: number;
  symbol: string;
}
