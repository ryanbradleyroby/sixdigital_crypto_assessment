export interface HistoryPayload {
  period: string;
  currency: string;
  crypto: string;
  limit: string;
}

export interface History {
  timefrom: number;
  timeto: number;
  data: Array<HistoryData>;
}

export interface HistoryData {
  time: number;
  high: number;
  low: number;
  open: number;
  volumefrom: number;
  volumeto: number;
  close: number;
  conversionType: 'direct';
  conversionSymbol: '';
}

export interface TimeFrames {
  type: string;
  value: string;
}
