import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private http: HttpClient) { }

  // Not in use
  // receiveData(): any {
  //   return this.socket.fromEvent('data');
  // }

  // // Not in use
  // send(): any {
  //   this.socket.emit('data');
  // }

  getOverview(): any {
    return this.http.get(environment.apipath);
  }

  getHistory(data): any {
    return this.http.post(environment.apipath, data);
  }
}
