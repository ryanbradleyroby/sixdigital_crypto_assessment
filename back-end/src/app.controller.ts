import { Body, Controller, Get, Post } from "@nestjs/common";
import { AppService, IHistory } from "./app.service";

@Controller("crypto")
export class AppController {
  constructor(private readonly appService: AppService) { }

  // @Get()
  // // not in use
  // getCrypto(): string {
  //   return this.appService.getCrypto();
  // }

  @Get()
  async getOverview(): Promise<any> {
    return this.appService.getOverview();
  }

  @Post()
  getHistory(@Body() data: IHistory): any {
    return this.appService.getHistory(data);
  }
}
