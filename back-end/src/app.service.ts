import { HttpService, Injectable } from "@nestjs/common";
import { map } from "rxjs/operators";
// import WebSocket = require("ws");

export interface IHistory {
  period: string;
  currency: string;
  crypto: string;
  limit: string;
}

@Injectable()
export class AppService {
  constructor(private http: HttpService) { }
  public data: any;

  // not in use
  // getCrypto(): any {
  //   const apiKey = "b480b1440b907676ac2cafdfcf0d16e44cc0316dfb55f9e8f8129b1c8c2b2166";
  //   const ccStreamer = new WebSocket("wss://streamer.cryptocompare.com/v2?api_key=" + apiKey);

  //   ccStreamer.on("open", function open() {
  //     const subRequest = {
  //       "action": "SubAdd",
  //       "subs": ["0~Coinbase~BTC~USD"]
  //     };
  //     ccStreamer.send(JSON.stringify(subRequest));
  //   });

  //   ccStreamer.on("message", function fetch(data: any) {
  //     // console.log(data);
  //   });
  // }

  getOverview(): any {
    return this.http.get("https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,LTC,BCH,ETC&tsyms=USD,GBP,EUR,JPY,ZAR").pipe(
      map(response => response.data),
    );
  }

  getHistory(data: IHistory): any {
    return this.http.get(`https://min-api.cryptocompare.com/data/v2/${data.period}?fsym=${data.crypto}&tsym=${data.currency}&limit=${data.limit}`).pipe(
      map(response => response.data),
    );
  }
}
